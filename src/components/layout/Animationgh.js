import React, { Fragment } from 'react';
import animationgh from './githubfinder.gif';

const Animationgh = () => {
  return (
    <Fragment>
      <img src={animationgh} alt="" className="home-animation" />
    </Fragment>
  );
};

export default Animationgh;
